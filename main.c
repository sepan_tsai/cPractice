#include "main.h"

int main() {
    int nums[] = {2, 7, 11, 15};
    int target = 9;
    int numsSize = sizeof(nums) / sizeof(nums[0]);
    int returnSize;
    int *answer = twoSum(nums, numsSize, target, &returnSize);
    printf("answer = [%d, %d]\n", *answer, *(answer+1));
    return 0;
}

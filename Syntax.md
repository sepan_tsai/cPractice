# C syntax

* int nums[] = {2,7,11,15};
* size_t num_length = sizeof(nums)/sizeof(nums[0]);
***
* Address:a &arr[i] == ptr+i
* Value: arr[i] == *(ptr+i)
* a) *ptr++ == *(ptr++) == arr[i++])。
* b) *++ptr == arr[++i])。
* c) ++*ptr == ++(*ptr) == ++arr[i])。
***
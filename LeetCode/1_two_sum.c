#include "../main.h"

/**
Note: The returned array must be malloced, assume caller calls free().
int nums[] = {2, 7, 11, 15};
int target = 9;
int numsSize = sizeof(nums) / sizeof(nums[0]);
int returnSize;
int *answer = twoSum(nums, numsSize, target, &returnSize);
printf("answer = [%d, %d]\n", *answer, *(answer+1));
 */
int *twoSum(const int *nums, int numsSize, int target, int *returnSize) {
    int *output = malloc(sizeof(int) * 2);
    *returnSize = 2;
    for (int i = 0; i < numsSize; i++) {
        for (int j = 0; j < numsSize; j++) {
            if (i == j) { continue; }
            if (nums[i] + nums[j] == target) {
                output[0] = i;
                output[1] = j;
                break;
            }
        }
    }
    printf("output[0] address => value = %p => %d\n", output, *output);
    printf("output[1] address => value = %p => %d\n", (output + 1), *(output + 1));
    return output;
}